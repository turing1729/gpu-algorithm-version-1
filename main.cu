#include "util.cuh"
#include <math.h>
#include <assert.h>
#include "kernel.cuh"
#include "cpu.cuh"

PerformanceTimer& timer()
{
    static PerformanceTimer timer;
    return timer;
}
const int t = 1;
int n_values[] = {512,32,64,128,256,512};
float cpu_iter_time[t];
float gpu_iter_time[t];
void csv(){
	char filename[]="stats.csv";
	FILE *fp;
	fp = fopen(filename,"w+");
	fprintf(fp,"No. Elements,CPU iterative,GPU iterative");
	for(int i=0;i<t;i++){
        fprintf(fp,"\n%d",n_values[i]);
		fprintf(fp,",%f",cpu_iter_time[i]);
		fprintf(fp,",%f",gpu_iter_time[i]);
	}
	fclose(fp);
	printf("\n %s file created\n",filename);
}	
bool check_validity(int *h_st,int *h_st_v,int N){
	bool tmp=true;
	for(int i=0;i<2*N;i++){
		printf("%d ",h_st[i]);
	}
	printf("\n");
	for(int i=0;i<2*N;i++){
		printf("%d ",h_st_v[i]);
	}
	printf("\n");
	for(int i=0;i<2*N;i++){
		if(h_st[i]!=h_st_v[i]){
			tmp=false;
			break;
		}
	}
	return tmp;
}
int main(){
	int N;
	for(int i=0;i<t;i++){
		N = n_values[i];
		int *h_st,*d_st,*d_arr,*h_arr,*h_st_verify;
		h_arr = (int*)calloc(N,sizeof(int));
		for(int j=0;j<N;j++){
			h_arr[j] = (rand()%10000) - ((rand()%10)*10);
		}
		assert(h_arr);
		h_st = (int*)calloc(2*N,sizeof(int));
		assert(h_st);
		h_st_verify = (int*)calloc(2*N,sizeof(int));
		assert(h_st_verify);
		cudaMalloc((void**)&d_st,2*N*sizeof(int));
		checkMallocError("cudaMalloc d_st failed!");
		cudaMalloc((void**)&d_arr,N*sizeof(int));
		checkMallocError("cudaMalloc d_arr failed!");
		cudaMemcpy(d_st,h_st,2*N*sizeof(int),cudaMemcpyHostToDevice);
		checkCUDAError("cudaMemcpy from h_st to d_st failed.");
		cudaMemcpy(d_arr,h_arr,N*sizeof(int),cudaMemcpyHostToDevice);
		checkCUDAError("cudaMemcpy from h_arr to d_arr failed.");
		float avg_gpu = 0.0f;
		int pOfTwo  = powerOfTwo(N);
        for(int j=0;j<10;j++){
            timer().startGpuTimer();
            construct_tree<<<dim3(1,1,1),dim3(pOfTwo>>1,1,1),2*N*sizeof(int)>>>(d_st,d_arr,N,pOfTwo);
            timer().endGpuTimer();
            avg_gpu+=timer().getGpuElapsedTimeForPreviousOperation();
        }
        gpu_iter_time[i] = avg_gpu/10;
		printf("GPU avg time for constructing the tree from an array of %d elements = %f ms\n",N,gpu_iter_time[i]);
        cudaMemcpy(h_st_verify,d_st,2*N*sizeof(int),cudaMemcpyDeviceToHost);
		
		float avg_cpu = 0.0f;
        for(int j=0;j<10;j++){
            timer().startCpuTimer();
            construct_tree_cpu(h_st,h_arr,N);
            timer().endCpuTimer();
            avg_cpu+=timer().getCpuElapsedTimeForPreviousOperation();
        }
        cpu_iter_time[i] = avg_cpu/10;
		printf("CPU avg time for constructing the tree from an array of %d elements = %f ms\n",N,cpu_iter_time[i]);
        bool verify;
		verify = check_validity(h_st,h_st_verify,N);
		printf("Goa vale beach pe\n");
		if(verify){
			printf("The CPU tree matches the GPU tree\n");
		}
		// cudaMemcpy(h_st,d_st,2*N*sizeof(int),cudaMemcpyDeviceToHost);
		free(h_arr);
		free(h_st);
		cudaFree(d_arr);
		checkFreeError("cudaFree d_arr failed.");
		cudaFree(d_st);
		checkFreeError("cudaFree d_st failed.");
    }
    // csv();
	return 0;
}
