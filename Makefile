#Makefile 
#define variables
objects= main.o kernel.o cpu.o util.o
NVCC= nvcc               #cuda c compiler
opt= -O2            #optimization flag
ARCH= -arch=sm_30        #cuda compute capability
LIBS=  -lm
execname= main


#compile
$(execname): $(objects)
	$(NVCC) $(opt) -o $(execname) $(objects) $(LIBS) 

kernel.o: kernel.cu
	$(NVCC) $(opt) $(ARCH) -c kernel.cu
main.o: main.cu
	$(NVCC) $(opt) $(ARCH) -std=c++11 -c main.cu
cpu.o: cpu.cu 
	$(NVCC) $(opt) $(ARCH) -std=c++11 -c cpu.cu
util.o: util.cu 
	$(NVCC) $(opt) $(ARCH) -std=c++11 -c util.cu

#clean Makefile
clean:
	rm $(objects)
#end of Makefile