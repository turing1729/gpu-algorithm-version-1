#ifndef __KERNEL_CUH__
#define __KERNEL_CUH__

__global__ void construct_tree(int *st,int* arr,int n,int pOfTwo);
int powerOfTwo(int x);
#endif