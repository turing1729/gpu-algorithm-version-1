#include "kernel.cuh"
#include<bits/stdc++.h>
int powerOfTwo(int x){
    int power = 1;
    while(power<x){
        power<<=1;
    }
    return power;
}
__global__
void construct_tree(int *st,int *arr,int n,int pOfTwo){
    extern __shared__ int temp[];
    int thid = threadIdx.x;
    int offset = 1;
    int offset2 = pOfTwo>>1;
    if((2*thid+pOfTwo)<(2*n)){
        temp[(2*thid)+pOfTwo] = arr[(2*thid+pOfTwo)-n]; 
        st[(2*thid)+pOfTwo] = arr[(2*thid+pOfTwo)-n];
        if((2*thid+1+pOfTwo)<(2*n)){
            temp[(2*thid+1)+pOfTwo] = arr[(2*thid+1+pOfTwo)-n]; 
            st[(2*thid+1)+pOfTwo] = arr[(2*thid+1+pOfTwo)-n];
        }
        temp[(2*thid)] = thid==0?0:INT_MAX;
        temp[(2*thid+1)] = thid==0?0:INT_MAX;
    }else{
        temp[(2*thid)+pOfTwo] = arr[(thid+(pOfTwo>>1))-n];
        temp[(2*thid+1)+pOfTwo] = arr[(thid+(pOfTwo>>1))-n];
        st[(thid+(pOfTwo>>1))] = arr[(thid+(pOfTwo>>1))-n];
        temp[(2*thid)] = INT_MAX;
        temp[(2*thid+1)] = INT_MAX; 
    }
    for(int d=pOfTwo>>1;d>0;d>>=1,offset2-=d){
        if(thid<d){
            int ai = offset*(2*thid+1)-1;
            int bi = offset*(2*thid+2)-1;
            atomicMin(&temp[bi+pOfTwo],temp[ai+pOfTwo]);
            temp[thid+offset2] = temp[bi+pOfTwo];
        }
        offset*=2;
    }
    if(2*thid<n){
        st[2*thid] = temp[2*thid];
        if(2*thid+1<n)
            st[2*thid+1] = temp[2*thid+1];
    }
}